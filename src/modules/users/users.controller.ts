import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { UserResponseDto } from './dto/user.response.dto';
import { CreateUserRequestDto } from './dto/createUser.request.dto';

@ApiTags('Users')
@Controller('users')
@ApiBearerAuth()
export class UsersController {
  constructor(private service: UsersService) {}

  @Get()
  @ApiCreatedResponse({
    description: 'You can get all users.',
    type: [UserResponseDto],
  })
  public async getAll(): Promise<UserResponseDto[]> {
    return await this.service.getAll();
  }

  @Get(':id')
  @ApiCreatedResponse({
    description: 'You can get user.',
    type: UserResponseDto,
  })
  public async getUser(@Param('id') id: number): Promise<UserResponseDto> {
    return await this.service.getUser(id);
  }

  @Post()
  @ApiCreatedResponse({
    description: 'You can create user.',
  })
  public async createUser(@Body() data: CreateUserRequestDto) {
    return await this.service.createUser(data);
  }
}
