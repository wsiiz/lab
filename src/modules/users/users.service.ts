import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../database/entities/user.entity';
import { Repository } from 'typeorm';
import { UserResponseDto } from './dto/user.response.dto';
import { CreateUserRequestDto } from './dto/createUser.request.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async getAll(): Promise<any> {
    return await this.userRepository.createQueryBuilder().getMany();
  }

  async getUser(id: number): Promise<UserResponseDto> {
    return await this.userRepository
      .createQueryBuilder()
      .where('id = :id', { id: id })
      .getOne()
      .then((user) => {
        if (user != null) {
          return new UserResponseDto(user);
        } else {
          throw new BadRequestException("Can't fetch user.");
        }
      });
  }

  async createUser(data: CreateUserRequestDto): Promise<void> {
    await this.userRepository.insert({
      email: data.email,
      passwordHash: 'none',
    });
  }
}
