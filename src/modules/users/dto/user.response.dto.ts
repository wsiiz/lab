import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { User } from 'src/database/entities/user.entity';

export class UserResponseDto {
  @IsNotEmpty()
  @ApiProperty()
  id: number;

  @IsString()
  @ApiProperty()
  email: string;

  constructor(entity: User) {
    this.id = entity.id;
    this.email = entity.email;
  }

  static getList(entities: User[]): UserResponseDto[] {
    return entities.map((entity) => {
      return new UserResponseDto(entity);
    });
  }
}
