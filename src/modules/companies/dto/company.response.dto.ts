import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { Company } from 'src/database/entities/company.entity';

export class CompanyResponseDto {
  @IsNotEmpty()
  @ApiProperty()
  id: number;

  @IsString()
  @ApiProperty()
  name: string;

  constructor(entity: Company) {
    this.id = entity.id;
    this.name = entity.name;
  }

  static getList(entities: Company[]): CompanyResponseDto[] {
    return entities.map((entity) => {
      return new CompanyResponseDto(entity);
    });
  }
}
