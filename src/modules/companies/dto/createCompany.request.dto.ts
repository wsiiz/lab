import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCompanyRequestDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  name: string;
}
