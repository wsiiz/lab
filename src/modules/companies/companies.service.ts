import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CompanyResponseDto } from './dto/company.response.dto';
import { Company } from 'src/database/entities/company.entity';
import { CreateCompanyRequestDto } from './dto/createCompany.request.dto';

@Injectable()
export class CompaniesService {
  constructor(
    @InjectRepository(Company)
    private companyRepository: Repository<Company>,
  ) {}

  async getAll(): Promise<any> {
    return await this.companyRepository.createQueryBuilder().getMany();
  }

  async getCompany(id: number): Promise<CompanyResponseDto> {
    return await this.companyRepository
      .createQueryBuilder()
      .where('id = :id', { id: id })
      .getOne()
      .then((company) => {
        if (company != null) {
          return new CompanyResponseDto(company);
        } else {
          throw new BadRequestException("Can't fetch user.");
        }
      });
  }

  async createCompany(data: CreateCompanyRequestDto): Promise<void> {
    await this.companyRepository.insert({
      name: data.name,
    });
  }
}
