import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { CompaniesService } from './companies.service';
import { CreateCompanyRequestDto } from './dto/createCompany.request.dto';
import { CompanyResponseDto } from './dto/company.response.dto';

@ApiTags('Companies')
@Controller('companies')
@ApiBearerAuth()
export class CompaniesController {
  constructor(private service: CompaniesService) {}

  @Get()
  @ApiCreatedResponse({
    description: 'You can get all companies.',
    type: [CompanyResponseDto],
  })
  public async getAll(): Promise<CompanyResponseDto[]> {
    return await this.service.getAll();
  }

  @Get(':id')
  @ApiCreatedResponse({
    description: 'You can get company.',
    type: CompanyResponseDto,
  })
  public async getCompany(
    @Param('id') id: number,
  ): Promise<CompanyResponseDto> {
    return await this.service.getCompany(id);
  }

  @Post()
  @ApiCreatedResponse({
    description: 'You can create company.',
  })
  public async createCompany(@Body() data: CreateCompanyRequestDto) {
    return await this.service.createCompany(data);
  }
}
